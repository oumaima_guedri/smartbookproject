package smartbook.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement
@Entity
public class  Utilisateur implements Serializable{
	
	@Id
    int cin;
	String nom;
	String prenom;
	String login;
	String password;
	String email;
	String adresse;
	int telephone;
	
	//Constructor

	public Utilisateur() {
		}
	//Getters&setters

	public String getNom() {
		return nom;
	}

	public int getCin() {
		return cin;
	}

	public void setCin(int cin) {
		this.cin = cin;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public int getTelephone() {
		return telephone;
	}

	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}

	@Override
	public String toString() {
		return "Utilisateur [cin=" + cin + ", nom=" + nom + ", prenom=" + prenom + ", login=" + login + ", password="
				+ password + ", email=" + email + ", adresse=" + adresse + ", telephone=" + telephone + "]";
	}
	
	
}
