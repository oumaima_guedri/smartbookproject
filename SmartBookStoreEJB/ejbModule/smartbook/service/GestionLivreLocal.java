package smartbook.service;

import java.util.List;

import javax.ejb.Local;

import smartbook.entities.livre;


@Local
public interface GestionLivreLocal {
	
	public void updateLivre (livre livre);
	public void deleteLivre (livre livre);
	public void addlivre(livre livre);
	public livre findlivreBynumSerie(int numSerie);
	public List<livre> findAlllivre();
}
