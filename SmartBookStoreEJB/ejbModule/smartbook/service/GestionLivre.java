package smartbook.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import smartbook.entities.Utilisateur;
import smartbook.entities.livre;


@Stateless
public class GestionLivre implements GestionLivreRemote, GestionLivreLocal{

	 @PersistenceContext( unitName = "SmartBookStoreEJB" )
	    private EntityManager em;

	@Override
	public void updateLivre(livre livre) {
		em.merge(livre);
		
	}

	@Override
	public void deleteLivre(livre livre) {
		em.remove(em.merge(livre));
		
	}

	@Override
	public void addlivre(livre livre) {
		em.persist(livre);
		
	}

	@Override
	public livre findlivreBynumSerie(int numSerie) {
		return em.find(livre.class, numSerie);
	}

	@Override
	public List<livre> findAlllivre() {
		Query et = em.createQuery("From livre");
		return et.getResultList();
	}

	
}
