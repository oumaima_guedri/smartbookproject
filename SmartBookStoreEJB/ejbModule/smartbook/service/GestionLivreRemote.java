package smartbook.service;

import java.util.List;

import javax.ejb.Remote;
import smartbook.entities.livre;


@Remote
public interface GestionLivreRemote {
	
	public void updateLivre (livre livre);
	public void deleteLivre (livre livre);
	public void addlivre(livre livre);
	public livre findlivreBynumSerie(int numSerie);
	public List<livre> findAlllivre();

}
