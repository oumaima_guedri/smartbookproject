package smartbook.service;

import java.util.List;
import javax.ejb.Local;

import smartbook.entities.Utilisateur;

@Local
public interface GestionUtilisateurLocal {

	void addUtilisateur(Utilisateur utilisateur) ;
	 public void updateEtudiant(Utilisateur utilisateur);
	 public Utilisateur findUtilisateurByCin(int cin);
	 public void deleteUtilisateur(Utilisateur utilisateur);
	 public List<Utilisateur> findAllUtilisateur();
	 public Utilisateur loginUtilisateur(String email, String password);
	 public Long countUtilisateur();
		public void confirmCompte(Utilisateur utilisateur);
}
