
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient} from '@angular/common/http';
import { Livre } from '../model/livre';

/*const httpOptions ={
    Headers : new HttpHeaders ( { 'Content-Type': 'appliction/json'})
};*/

@Injectable({
  providedIn: 'root'
})
export class LivreService {
    urlpath :string;
  // getLivre : string;

    constructor(private http:HttpClient) {
        this.urlpath='http://localhost:8484/SmartBookStoreREST/rest/livrecrud/';


        // this.getLivre = 'http://localhost:8484/SmartBookStoreREST/rest/livrecrud/all1'
   /* Livres :Livre[];*/

  /*  ListeLivre():Observable <livre[]>{
        return this.Http.get<livre[]>(this.apiURL);
        }*/
   }
   getAllLivre()//:Observable<Livre[]>
   {
      // return this.http.get<Livre[]>(this.getLivre);
       return this.http.get(this.urlpath + 'all1');

   }
   getLivreBynumSerie(numSerie:number):Observable<any>
   {
       return this.http.get(this.urlpath +'bynum/' +numSerie);
   }
   supprimerLivre(numSerie: number): Observable<any>
   {
     return this.http.delete(this.urlpath + 'D1' + numSerie, { responseType: 'text' });
   }

  addLivre(l:Livre):Observable<Object>
  {
    return this.http.post(this.urlpath + 'add', l);
  }

  updateLivre(l:Livre): any
  {
    return this.http.put(this.urlpath + 'update', l);
  }



  /* ajouterLivre( prod: Livre):Observable<Livre>{
    return this.http.post<Livre>(this.apiURL, prod, httpOptions);
    }*/
}
