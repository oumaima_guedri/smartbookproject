import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Lecteurs } from '../model/lecteurs';

@Injectable({
  providedIn: 'root'
})
export class LecteurService {
 
  urlpath :string;

  constructor(private http:HttpClient) {
    this.urlpath='http://localhost:8484/SmartBookStoreREST/rest/usercrud/';
   }
   getAllLecteur()
   {
     return this.http.get(this.urlpath +'all');
   }
 
 
    getLecteurBycin(cin: number): Observable<any>
   {
     return this.http.get(this.urlpath + 'cin' + cin);
   }
 
   supprimerLecteur(cin: number): Observable<any>
   {
     return this.http.delete(this.urlpath + 'D1' + cin, { responseType: 'text' });
   }
 
 
   addLecteur(l: Lecteurs): Observable<Object>
   {
     return this.http.post(this.urlpath + 'inscrit', l);
   }
 
 
   updateLecteur(l: Lecteurs): any
   {
     return this.http.put(this.urlpath + 'update', l);
   }
 }
