import { Router } from '@angular/router';
import { LibraireService } from './../services/libraire.service';
import { libraire } from './../model/libraire';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-listelibraire',
  templateUrl: './listelibraire.component.html',
  styleUrls: ['./listelibraire.component.scss']
})
export class ListelibraireComponent implements OnInit {


    libraire: libraire [] = new Array;

    constructor(private LibraireService: LibraireService,
                private router: Router) { }




    ngOnInit(): void {
      this.reloadData();
    }

    reloadData(): void {
      // @ts-ignore
      this.libraire = this.libraireService.getAllLibraire();
    }

    deleteLibraire(cin: number): void
    {
      this.LibraireService.supprimerLibraire(cin)
        .subscribe(data => {
          console.log(data);
          this.reloadData();
        },
          error => console.log(error));
    }

    ajouter(): void {
  this.router.navigate(['/addlibraire']);
    }

    toUpdate(cin: number): void {
      this.router.navigate(['/update', cin]);

    }


}
